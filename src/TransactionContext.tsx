import React, { useState, useContext } from 'react'
import { AccountType } from './types'

type Context = {
  toggleModal: (account?: AccountType) => void
  showingModal: boolean
  account: AccountType
}

const dummyCTX: Context = {
  toggleModal: () => {},
  showingModal: false,
  account: 'crap',
}

export const TransactionContext = React.createContext<Context>(dummyCTX)
export const TransactionContextProvider = (props: any) => {
  const [showingModal, setShowingModal] = useState(false)
  const [account, setAccount] = useState<AccountType>('crap')
  const toggleModal = (acc: AccountType) => {
    setAccount(acc)
    setShowingModal(!showingModal)
  }

  return (
    <TransactionContext.Provider
      value={{ account, toggleModal, showingModal }}
      {...props}
    />
  )
}

export const useTransactionContext = () => useContext(TransactionContext)
