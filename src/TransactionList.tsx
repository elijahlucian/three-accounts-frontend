import React, { useContext } from 'react'
import { AppContext } from './AppContext'
import { accountTypes } from './'
import './TransactionList.scss'

export const TransactionList = () => {
  const appContext = useContext(AppContext)

  return (
    <>
      <h1>Transactions</h1>
      <div className="transactions">
        {accountTypes.map((account, i) => {
          return (
            <div className="column" key={`${account}-txs-${i}`}>
              <h3>{account}</h3>
              {appContext?.balances &&
                appContext.balances[account].map(tx => (
                  <p className="transaction-item">
                    <p className="amount">${tx.amount}</p>
                    <p className="details">{tx.details}</p>
                    <button
                      onClick={() =>
                        tx.id && appContext.deleteTransaction(tx.id)
                      }
                    >
                      X
                    </button>
                  </p>
                ))}
            </div>
          )
        })}
      </div>
    </>
  )
}
