import React, { useState, useContext, useEffect } from 'react'

type StatusType = 'error' | 'good' | 'off'

type Context = {
  getStatus: () => JSX.Element
  setStatus: (message: string, type: StatusType) => void
}

export const StatusContext = React.createContext<Context | null>(null)

export const StatusContextProvider = (props: any) => {
  const [message, setMessage] = useState<string | null>(null)
  const [type, setType] = useState<string | null>(null)

  // create status bar with useEffect
  // timeout statusbar 1 second.

  const setStatus = (message: string, type: StatusType) => {
    setMessage(message)
    setType(type)
  }

  const getStatus = () => {
    setTimeout(() => {
      setType('off')
    }, 1000)
    return <div className={`status ${type}`}>{message}</div>
  }

  return <StatusContext.Provider value={{ getStatus, setStatus }} {...props} />
}

export const useTransactionContext = () => useContext(StatusContext)
