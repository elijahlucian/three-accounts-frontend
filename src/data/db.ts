import { Profile, Transaction, User } from '../types'

export const profileIndex: Record<string, Profile> = {
  kande: {
    id: 'kande',
    name: 'Kaela & Elijah',
    details: 'blah',
    groceries: 800,
    crap: 1000,
    takeout: 500,
    income: 5200,
    expenses: 1900,
  },
}

export const userIndex: Record<string, User> = {
  elijah: {
    id: 'elijah',
    email: '',
    password: '',
    name: 'Elijah',
    accounts: [profileIndex.kande],
  },
  kaela: {
    id: 'kaela',
    email: '',
    password: '',
    name: 'Kaela',
    accounts: [profileIndex.kande],
  },
}

export const txIndex: Record<string, Transaction[]> = {
  kande: [
    { id: 1, accountName: 'groceries', amount: 400, details: 'halfmonth' },
    { id: 2, accountName: 'groceries', amount: 122, details: 'superstore' },
    { id: 3, accountName: 'groceries', amount: 21.71, details: 'community' },
    { id: 4, accountName: 'groceries', amount: 75.08, details: 'sobeys' },
    { id: 5, accountName: 'groceries', amount: 32.56, details: 'sobeys' },
    { id: 6, accountName: 'crap', amount: 500, details: 'halfmonth' },
    { id: 7, accountName: 'crap', amount: 149.09, details: 'vechicle' },
    { id: 8, accountName: 'crap', amount: 20, details: 'starbucks' },
    { id: 9, accountName: 'crap', amount: 20.81, details: 'thriftstore' },
    { id: 10, accountName: 'crap', amount: 47, details: 'gas rav4' },
    { id: 11, accountName: 'crap', amount: 40, details: 'gas van' },
    { id: 12, accountName: 'takeout', amount: 250, details: 'halfmonth' },
    { id: 13, accountName: 'takeout', amount: 47.12, details: 'raj palace' },
    { id: 14, accountName: 'takeout', amount: 14.66, details: '' },
    { id: 15, accountName: 'takeout', amount: 18.48, details: 'safari grill' },
    { id: 16, accountName: 'takeout', amount: 72.82, details: 'Li Ao' },
    { id: 17, accountName: 'takeout', amount: 13.58, details: 'Thai Express' },
  ],
}
