import React, { useContext } from 'react'
import { User } from './types'
import { Link } from 'react-router-dom'
import { StatusContext } from './StatusContext'
import './AccountSelect.scss'

type Props = {
  user: User
  selectProfile: (id: string) => void
}

export const AccountSelect = ({ user, selectProfile }: Props) => {
  const statusContext = useContext(StatusContext)

  const handleSelect = (id: string) => {
    selectProfile(id)
    statusContext?.setStatus(`Selected Account: ${id}`, 'good')
  }

  return (
    <>
      <h1>Select Budget</h1>
      <div className="account-select">
        {user.accounts?.length
          ? user.accounts.map(profile => (
              <button
                key={`profile-${profile.name}`}
                onClick={() => handleSelect(profile.id)}
              >
                {profile.name}
              </button>
            ))
          : ''}
        <Link to="/account/new">Create New Budget!</Link>
      </div>
    </>
  )
}
