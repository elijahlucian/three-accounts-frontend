import React, { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import axios from 'axios'
import { userIndex } from './data/db'

import { App } from './App'
import { Login } from './Login'
import { AppContextProvider } from './AppContext'
import * as serviceWorker from './serviceWorker'
import './index.css'
import { AccountType, User } from './types'
import { TransactionContextProvider } from './TransactionContext'
import { UserForm } from './UserForm'
import { NavContextProvider } from './NavContext'
import { StatusContextProvider } from './StatusContext'

export const accountTypes: AccountType[] = ['groceries', 'takeout', 'crap']

const AppWithContext = () => {
  const [user, setUser] = useState<User>()

  const handleLogin = async (id: string) => {
    const { data } = await axios.get(`/api/users/${id}/`)
    setUser(data)
  }

  return !user ? (
    <BrowserRouter>
      <Switch>
        <Route path="/sign-up">
          <UserForm handleLogin={handleLogin} />
        </Route>
        <Route path="/">
          <Login handleLogin={handleLogin} />
        </Route>
      </Switch>
    </BrowserRouter>
  ) : (
    <StatusContextProvider>
      <NavContextProvider>
        <AppContextProvider user={user}>
          <TransactionContextProvider>
            <App />
          </TransactionContextProvider>
        </AppContextProvider>
      </NavContextProvider>
    </StatusContextProvider>
  )
}

ReactDOM.render(<AppWithContext />, document.getElementById('root'))
serviceWorker.unregister()
