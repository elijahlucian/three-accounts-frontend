export type AccountType = 'groceries' | 'takeout' | 'crap'

export type CalculatedAccountIndex = Record<AccountType, Transaction[]>

export type AccountIndex = Record<AccountType, number>

export type Transaction = {
  // Q: where account_id = user_id, and time between month.start & month.end
  id?: number
  amount: number
  details: string
  accountName: AccountType
  accountId?: string
}

export type Profile = AccountIndex & {
  id: string
  name: string
  income: number
  expenses: number
  details?: string
  // transactions: Transaction[]
}

export type User = {
  id: string
  name: string
  email: string
  password: string
  accounts: Profile[]
}
