import React, { useState, useContext } from 'react'
import { Profile, User } from './types'
import axios from 'axios'
import './DankForm.scss'
import { StatusContext } from './StatusContext'
import { AppContext } from './AppContext'

type Props = {
  profile?: Profile
  user: User
}

export const AccountForm = ({ profile, user }: Props) => {
  const statusContext = useContext(StatusContext)
  const appContext = useContext(AppContext)

  const [name, setName] = useState(profile?.name)
  const [crap, setCrap] = useState(profile?.crap)
  const [groceries, setGroceries] = useState(profile?.groceries)
  const [takeout, setTakeout] = useState(profile?.takeout)
  const [details, setDetails] = useState(profile?.details)
  const [income, setIncome] = useState(profile?.income)
  const [expenses, setExpenses] = useState(profile?.expenses)

  // @ts-ignore
  const handleSubmit = async e => {
    e.preventDefault()
    if (!name || !crap || !groceries || !takeout || !income || !expenses)
      statusContext?.setStatus('fill out all the fields!', 'error')

    const body = {
      name,
      crap,
      groceries,
      takeout,
      income,
      expenses,
      details,
    }

    profile?.id
      ? await appContext?.updateProfile(profile.id, body as Profile)
      : await appContext?.createProfile(body as Profile)

    statusContext?.setStatus('Saved!', 'good')
  }

  return (
    <>
      <h1>{profile?.id ? 'Budget Details' : 'Create Budget'}</h1>
      <form className="dank-form" onSubmit={handleSubmit}>
        <h3>General</h3>
        <label>
          Name:
          <input onChange={e => setName(e.target.value)} value={name}></input>
        </label>
        <label>
          Description (optional):
          <input
            type="text"
            onChange={e => setDetails(e.target.value)}
            value={details}
          ></input>
        </label>
        <h3>Income &amp; Expenses</h3>
        <label>
          Monthly Income:
          <input
            onChange={e => setIncome(parseInt(e.target.value) || undefined)}
            value={income}
          ></input>
        </label>
        <label>
          Monthly Expenses:
          <input
            onChange={e => setExpenses(parseInt(e.target.value) || undefined)}
            value={expenses}
          ></input>
        </label>
        <h3>Budgets</h3>
        <label>
          Groceries:
          <input
            onChange={e => setGroceries(parseInt(e.target.value) || undefined)}
            value={groceries}
          ></input>
        </label>
        <label>
          Takeout:
          <input
            onChange={e => setTakeout(parseInt(e.target.value) || undefined)}
            value={takeout}
          ></input>
        </label>
        <label>
          Crap:
          <input
            onChange={e => setCrap(parseInt(e.target.value) || undefined)}
            value={crap}
          ></input>
        </label>
        <button type="submit">{profile?.id ? 'Save' : 'Create'}</button>
      </form>
    </>
  )
}
